# Simulation Model Version 0.0 Using Randomly Created data 

from random import normalvariate as stdNorm
from matplotlib import pyplot as plt
import time


def Growth_Velocity_Generation(Age, Growth_Velocity_Mean, Growth_Velocity_SD):

	Mean = Growth_Velocity_Mean[Age]
	SD   = Growth_Velocity_SD[Age]

	Growth_Velocity = stdNorm(Mean,SD)

	return Growth_Velocity

def Diarrhoea_Predictor_Generator(Diarrhoea_Mean, Diarrhoea_SD):
	return stdNorm(Diarrhoea_Mean, Diarrhoea_SD) 


def main(Simulations):
	#						 0.5    1.0     1.5   2.0    2.5    3.0    3.5    4.0    4.5    5.0    5.5   6.0    6.5    7.0    7.5    8.0    8.5    9.0    9.5   10.0
	Growth_Velocity_Mean = [50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13]
	Growth_Velocity_SD   = [5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11]

	Diarrhoea_Mean = 9.2
	Diarrhoea_SD   = 3.1

	Growth_Velocity_Data = [0]*20

	for i in range(Simulations):
		for Age in range(0,20):
			Growth_Velocity = Growth_Velocity_Generation(Age, Growth_Velocity_Mean, Growth_Velocity_SD)
			Growth_Velocity -= Diarrhoea_Predictor_Generator(Diarrhoea_Mean, Diarrhoea_SD)

			Growth_Velocity_Data[Age] += (Growth_Velocity)

	for i in range(0,20):
		Growth_Velocity_Data[i] /= Simulations

	return Growth_Velocity_Data

start = time.time()

Variance_Data = []

for i in range(1000):
	Variance_Data.append(main(1000))

Max_Var = [0] * 20
Min_Var = [100] * 20
for i in range(len(Variance_Data)):
	for j in range(20):

		if Variance_Data[i][j] > Max_Var[j]:
			Max_Var[j] = Variance_Data[i][j]

		if Variance_Data[i][j] < Min_Var[j]:
			Min_Var[j] = Variance_Data[i][j]

Growth_Velocity_Data = [0] * 20

for i in range(len(Variance_Data)):
	for j in range(20):
		Growth_Velocity_Data[j] += Variance_Data[i][j]

for i in range(len(Growth_Velocity_Data)):
	Growth_Velocity_Data[i] /= len(Variance_Data)

# print "Simulations took: {} seconds".format(time.time() - start)

# Age = 0.5
# for i in range(0,20):
# 	print "Age {} = {}".format(Age,Growth_Velocity_Data[i])
# 	Age += 0.5

plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Growth_Velocity_Data, 'k')

plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Max_Var, 'r')

plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Min_Var, 'g')

plt.show()

