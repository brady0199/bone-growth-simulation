#!/usr/bin/env python
"""
	Description:  Provides an implementation of the predictor class
"""

__version__ = '0.1'
__author__ = 'Brady Woolston'

from matplotlib import pyplot as plt
from random import normalvariate as stdNorm
import time

class Predictor:

	def __init__(self, name, mean, sd):
		self.name = name
		self.mean = mean
		self.sd = sd
	
	def __str__(self):
		return "Name: {}\nMean: {}\nSD:   {}".format(self.name, self.mean, self.sd)

	def generate(self):
		Prediction_Data = [0] * len(self.mean) # Pre allocate memory for array

		for i, mean, sd in zip(range(len(self.mean)), self.mean, self.sd):
			Prediction_Data[i] = stdNorm(mean, sd)

		return Prediction_Vector(Prediction_Data)

class Prediction_Vector:
	def __init__(self, data):
		self.data = data
	
	def __str__(self):
		return "Data: {}".format(self.data)
	
	def __sub__(self, deficit):
		if isinstance(deficit, Prediction_Vector) and len(deficit.data) == len(self.data):
			result = [0] * len(self.data)
			for i,x,y in zip(range(len(self.data)), self.data, deficit.data):
				result[i] = x-y
			
			return Prediction_Vector(result)
	
	def __add__(self, deficit):
		if isinstance(deficit, Prediction_Vector) and len(deficit.data) == len(self.data):
			result = [0] * len(self.data)
			for i,x,y in zip(range(len(self.data)), self.data, deficit.data):
				result[i] = x+y
			
			return Prediction_Vector(result)
	
	def __truediv__(self, divisor):
		result = [0] * len(self.data)
		for i in range(len(self.data)):
			result[i] = self.data[i] / divisor
		return result


class Model:
	def __init__(self, base_predictor, deficit_predictor, generations):
		self.base_predictor = base_predictor
		self.deficit_predictor = deficit_predictor
		self.generations = generations
	
	def __str__(self):
		return "Model:\nBase Predictor: {}\nDeficit Predictor: {}\nGenerations: {}".format(self.base_predictor, self.deficit_predictor, self.generations)

	def generate_model(self):
		data = self.base_predictor.generate() - self.deficit_predictor.generate()
		for _ in range(1,self.generations):
			data = data + (self.base_predictor.generate() - self.deficit_predictor.generate())
		
		return Prediction_Vector(data/self.generations)

class Simulation:

	def __init__(self, model, generations):
		self.model = model
		self.generations = generations

	def simulate(self):
		self.data = self.model.generate_model()
		for i in range(1, self.generations):
			#print("Simulation Number {}".format(i))
			self.data = self.data + self.model.generate_model()

		self.data = self.data/self.generations

	def plot(self):
		plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],self.data, 'k')
		plt.show()



Normal_Growth = Predictor("Normal Growth Predictor",
						[50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13],
						[5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11])

Dioreaha = Predictor("Dioreaha Predictor",
					[9.2, 9.2, 9.2, 9.2, 8.0, 7.6, 7.4, 6.9, 6.6, 6.2, 5.0, 5.0, 5.0, 5.0, 5.0, 4.0, 4.0, 4.0, 4.0, 4.0],
					[0.5, 0.5, 0.5, 0.5, 1.1, 1.1, 1.5, 1.8, 2.1, 2.2, 2.6, 2.9, 3.5, 3.9, 4 ,4.2, 4.2, 4.2, 4.2,  4.2 ])

Dioreaha_Model = Model(Normal_Growth, Dioreaha, 100)

Dioreaha_Simulation = Simulation(Dioreaha_Model, 1000)
start = time.time()
Dioreaha_Simulation.simulate()
print("Executed in {}".format(time.time()-start))
Dioreaha_Simulation.plot()
	

		