#!/usr/bin/env python
"""
    Description: This script runs a number of simulations about the effects on 
    Version:    1.2
    Python:     Writen For Python 3+
    Author:     Brady Woolston   
"""

from random import normalvariate as stdNorm
import sqlite3

try:
    from matplotlib import pyplot as plt
except ImportError:
    print("*"*80)
    print('WARNING:\nDependency matplotlib missing no graphical output can be produced')
    print('Call "pip install matplotlib" to fix dependance error')
    print("*"*80)

def frange(x ,y ,step):
    Array = [0] * int((y-x) * (1/step))
    count = 0

    while y > x:
        Array[count] = x
        x += step
        count += 1

    Array.append(y)
    return Array 


class Menu:
    def __init__(self):

        self.Main_Menu = ['Saved Simulations','New Simulation','New Predictor','Growth Model Options', 'Run Simulation']
        self.New_Simulations_Menu = ['Main Menu','Add Predictor','Remove Predictor', 'Change Growth Model','Save Simulation', 'New Predictor']
        self.Growth_Model_Menu = ['Main Menu', 'Edit defult', 'New Growth Model', 'Delete Growth Model']
        self.Saved_Simulations_Menu = self._load_simulations()

        # self.load_predictors()

        self.previous_function = 'Main Menu'

        self.Functions_Map = {'Main Menu':self._main_menu,
                              'New Simulation':self._new_simulation,
                              'New Predictor':self._new_predictor,
                              'Growth Model Options':self._growth_model_options}

        self._main_menu()


    ''' Core Functions '''

    def _load_simulations(self):
        pass

    def _format(self, menu):
        for i in range(0, len(menu)):
            print("[{}] {}".format(i+1, menu[i-1]))

    def _selection_handler(self, menu):
        try:
            Selection = int(input('> '))
            if (Selection - 1) > len(menu):
                raise ValueError
        except ValueError:
            print('Command was not recognised')

        self.Functions_Map[menu[Selection-1]]()


    ''' Menu Functions'''

    def _main_menu(self):
        self._format(self.Main_Menu)
        self.previous_function = 'Main Menu'
        self._selection_handler(self.Main_Menu)

    def _saved_simulations(self):
            pass

    def _new_simulation(self):
        self._format(self.New_Simulations_Menu)
        self.previous_function = 'New Simulation'
        self._selection_handler(self.New_Simulations_Menu)

    def _growth_model_options(self):
        self._format(self.Growth_Model_Menu)
        self.previous_function = 'Growth Model Options'
        self._selection_handler(self.Growth_Model_Menu)

    ''' Menu Item Functions '''

    def _new_predictor(self):
        Predictor().New_Predictor()
        self.Functions_Map[self.previous_function]()

    def _run_simulation(self):
        pass


class Predictor:

    def __init__(self,Name='',Age_Range=[],Predictor_Mean=[],Predictor_SD=[]):
        self.Name = Name
        self.Age_Range = Age_Range
        self.Predictor_Mean = Predictor_Mean
        self.Predictor_SD = Predictor_SD
        
    def New_Predictor(self):
            self.name = input('Predictor Name: ')

            Start_Age = float(input('Start Age: '))
            End_Age = float(input('End Age: '))
            Age_Increment = float(input('Age increment (Years): '))

            self.Age_Range = frange(Start_Age, End_Age, Age_Increment)
            self.Predictor_Mean = [0] * len(self.Age_Range)
            self.Predictor_SD = [0] * len(self.Age_Range)

            for i in range(0,len(self.Age_Range)):
                    self.Predictor_Mean[i] = float(input('Enter {} year mean: '.format(self.Age_Range[i])))
                    self.Predictor_SD[i] = float(input('Enter {} year standard deviation: '.format(self.Age_Range[i])))

            if input("Would you like to save this predictor (y/n): ").lower() == 'y':
                self._save_predictor()
            else:
                print('Predictor was discarded')

    def _save_predictor(self):
            conn, cursor = DB_Connect()
            cursor.execute("CREATE TABLE {} (Age int,Mean int,SD int)".format(self.name))
            conn.commit()
            for i in range(len(self.Age_Range)):
                cursor.execute("INSERT INTO {} VALUES(?,?,?)".format(self.name),(self.Age_Range[i],self.Predictor_Mean[i],self.Predictor_SD[i]))
                conn.commit()
            conn.close()

            print ('Predictor successfully saved')

class Growth_Model:
    def Edit_Defult(self):
        pass

    def New_Growth_Model(self):
        pass

    def Save_Growth_Model(self):
        pass


class Simulation:
    def __init__(self):
        pass


def DB_Connect():
    conn = sqlite3.connect('Simulation_DataBase.db')
    return conn, conn.cursor()


# conn = DB_Connect()

# names =  conn.execute("SELECT * FROM [Saved_Simulation_Names]").fetchall()

# print names[0][0]

# print conn.execute("SELECT * FROM [{}]".format(names[0][0])).fetchall()


Menu()




