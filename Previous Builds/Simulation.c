#include <math.h>
#include <stdlib.h>
#include <time.h>


double stdNorm (double mu, double sigma)
{
  double U1, U2, W, mult;
  static double X1, X2;
  static int call = 0;

  if (call == 1)
    {
      call = !call;
      return (mu + sigma * (double) X2);
    }

  do
    {
      U1 = -1 + ((double) rand () / RAND_MAX) * 2;
      U2 = -1 + ((double) rand () / RAND_MAX) * 2;
      W = pow (U1, 2) + pow (U2, 2);
    }
  while (W >= 1 || W == 0);

  mult = sqrt ((-2 * log (W)) / W);
  X1 = U1 * mult;
  X2 = U2 * mult;

  call = !call;

  return (mu + sigma * (double) X1);
}

double Growth_Velocity_Generation(int Age, float Growth_Velocity_Mean[], float Growth_Velocity_SD[])
{
  return stdNorm(Growth_Velocity_Mean[Age], Growth_Velocity_SD[Age]);
}

double Diarrhoea_Predictor_Generator(int Age, float Diarrhoea_Mean[], float Diarrhoea_SD[])
{
  return stdNorm(Diarrhoea_Mean[Age], Diarrhoea_SD[Age]);
}

void Diarrhoea_Model(int Simulations, float Growth_Velocity_Data[20])
{

  float Growth_Velocity = 0.0;

  float Growth_Velocity_Mean[20] = {50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13};
  float Growth_Velocity_SD[20]   = {5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11};

  float Diarrhoea_Mean[20] =       {9.2, 9.2, 9.2, 9.2, 8.0, 7.6, 7.4, 6.9, 6.6, 6.2, 5.0, 5.0, 5.0, 5.0, 5.0, 4.0, 4.0, 4.0, 4.0, 4.0};
  float Diarrhoea_SD[20]   =       {0.5, 0.5, 0.5, 0.5, 1.1, 1.1, 1.5, 1.8, 2.1, 2.2, 2.6, 2.9, 3.5, 3.9, 4 ,4.2, 4.2, 4.2, 4.2,  4.2 };

  for(int i=0; i<Simulations; i++)
  {
    for(int Age=0; Age<20; Age++)
    {
      Growth_Velocity = Growth_Velocity_Generation(Age, Growth_Velocity_Mean, Growth_Velocity_SD);
      Growth_Velocity -= Diarrhoea_Predictor_Generator(Age, Diarrhoea_Mean, Diarrhoea_SD);

      Growth_Velocity_Data[Age] += Growth_Velocity;
    }
  }

  for(int i=0; i<20; i++)
  {
    Growth_Velocity_Data[i] /= Simulations;
  }

}


int main(void)
{
  float Growth_Velocity_Data[20] = {0};
  time_t start;
  time_t finish;

  srand(time(0));
  start = time(NULL);
  Diarrhoea_Model(500000,Growth_Velocity_Data);
  finish = time(NULL);

  printf("Executed in %ld seconds\n",finish-start);
  printf("[");
  for(int i=0; i<20;i++)
  {
    printf("%f,",Growth_Velocity_Data[i]);
  }
  printf("]\n");
  return 0;
}