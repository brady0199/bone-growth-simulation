#!/usr/bin/env python
"""
	Description: This script runs a number of simulations about the effects on 
	Version: 1.2
	Author:  Brady Woolston
	
"""


from random import normalvariate as stdNorm
from matplotlib import pyplot as plt
import time

def Growth_Velocity_Generation(Age, Growth_Velocity_Mean, Growth_Velocity_SD):
	
	'''
		Index the specific Growth related arrays in order to get the age 
		Specific Mean and Standard Deviation
		Calculate Growth velocity using a Standard Normal Distribution 
		Using the age specific Mean and Standard Deviation
	'''

	return stdNorm(Growth_Velocity_Mean[Age],Growth_Velocity_SD[Age])


def Diarrhoea_Predictor_Generator(Age, Diarrhoea_Mean, Diarrhoea_SD):
	return stdNorm(Diarrhoea_Mean[Age], Diarrhoea_SD[Age]) 


def Diarrhoea_Model(Simulations):
	
	#						 0.5    1.0     1.5   2.0    2.5    3.0    3.5    4.0    4.5    5.0    5.5   6.0    6.5    7.0    7.5    8.0    8.5    9.0    9.5   10.0
	Growth_Velocity_Mean = [50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13]
	Growth_Velocity_SD   = [5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11]

	#						0.5  1.0  1.5  2.0  2.5  3.0  3.5  4.0  4.5  5.0  5.5  6.0  6.5  7.0  7.5  8.0  8.5  9.0  9.5  10.0
	Diarrhoea_Mean = 	   [9.2, 9.2, 9.2, 9.2, 8.0, 7.6, 7.4, 6.9, 6.6, 6.2, 5.0, 5.0, 5.0, 5.0, 5.0, 4.0, 4.0, 4.0, 4.0, 4.0]
	Diarrhoea_SD   =       [0.5, 0.5, 0.5, 0.5, 1.1, 1.1, 1.5, 1.8, 2.1, 2.2, 2.6, 2.9, 3.5, 3.9, 4 ,4.2, 4.2, 4.2, 4.2,  4.2 ]

	Growth_Velocity_Data = [0]*20

	for _ in range(Simulations):
		for Age in range(0,20):
			Growth_Velocity = Growth_Velocity_Generation(Age, Growth_Velocity_Mean, Growth_Velocity_SD)
			Growth_Velocity -= Diarrhoea_Predictor_Generator(Age, Diarrhoea_Mean, Diarrhoea_SD)

			#print('Growth velocity data for point {}')

			Growth_Velocity_Data[Age] += (Growth_Velocity)

	for i in range(0,20):
		Growth_Velocity_Data[i] /= Simulations

	return Growth_Velocity_Data

def Diarrhoea_Simulation(Models, Simulations):
	Variance_Data = []

	for i in range(Models):
		Variance_Data.append(Diarrhoea_Model(Simulations))

	Max_Var = [0] * 20
	Min_Var = [100] * 20

	for i in range(len(Variance_Data)):
		for j in range(20):

			if Variance_Data[i][j] > Max_Var[j]:
				Max_Var[j] = Variance_Data[i][j]

			if Variance_Data[i][j] < Min_Var[j]:
				Min_Var[j] = Variance_Data[i][j]

	Growth_Velocity_Data = [0] * 20

	for i in range(len(Variance_Data)):
		for j in range(20):
			Growth_Velocity_Data[j] += Variance_Data[i][j]

	for i in range(len(Growth_Velocity_Data)):
		Growth_Velocity_Data[i] /= len(Variance_Data)

	return Growth_Velocity_Data, Max_Var, Min_Var


def Normal_Growth_Model(Simulations):
	#						 0.5    1.0     1.5   2.0    2.5    3.0    3.5    4.0    4.5    5.0    5.5   6.0    6.5    7.0    7.5    8.0    8.5    9.0    9.5   10.0
	Growth_Velocity_Mean = [50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13]
	Growth_Velocity_SD   = [5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11]

	Growth_Velocity_Data = [0]*20

	for i in range(Simulations):
		for Age in range(0,20):
			Growth_Velocity = Growth_Velocity_Generation(Age, Growth_Velocity_Mean, Growth_Velocity_SD)

			Growth_Velocity_Data[Age] += (Growth_Velocity)

	for i in range(0,20):
		Growth_Velocity_Data[i] /= Simulations

	return Growth_Velocity_Data


def plot(Growth_Velocity_Data, Max_Var, Min_Var, Norm_Growth_Velocity):
	
	c_data = [39.619419,27.711578,21.955893,18.715481,15.986628,13.371407,12.774036,12.842106,12.292663,12.909757,14.440029,13.318757,13.180051,13.928372,13.887883,13.741593,15.843325,15.701360,13.535528,11.125034]

	plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Growth_Velocity_Data, 'k')
	plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Max_Var, 'r')
	plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Min_Var, 'g')
	plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],Norm_Growth_Velocity, 'b')

	#plt.plot([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],c_data, 'g--')



	plt.title("Bone Growth Simulation with the effects of Diarrhoea")
	plt.xlabel('Age')
	plt.ylabel('Bone Growth Velocity (mm/year)')

	plt.xticks([0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10])
	plt.legend(['Mean','Max Var','Min Var','Normal Growth'])

	plt.show()


def main():
	start = time.time()
	N_Models_Diarrhoea = 100
	N_Simulations_Diarrhoea = 1000

	N_Simulations_Norm = 1000

	Growth_Velocity_Data, Max_Var, Min_Var = Diarrhoea_Simulation(N_Models_Diarrhoea,N_Simulations_Diarrhoea)
	Norm_Growth_Velocity = Normal_Growth_Model(N_Simulations_Norm)
	print("Executed in {}".format(time.time()-start))
	plot(Growth_Velocity_Data, Max_Var, Min_Var, Norm_Growth_Velocity)

if __name__ == '__main__':
	main()
	
