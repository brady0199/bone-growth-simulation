# Simulation Builder

## Latest Commit: Version 6.0

## Latest Changes 
+ ### Code written for Version 5 and below will no longer work with the Version 6 update due to plotting changes.
+ ### A Complete change of how the plotting method works. See [Plotting](#plotexample) for usage instructions
+ ### A update to the toCSV function that now includes the max and min variance when avaliable

## Version 5 Changes
+ ### Simulate now takes an optional *variance* argument see [Variance Example](#variance) for usage instructions.
+ ### Multiple simulations can be plotted on the same figure. See [Multiple Plots](#multiplots) for usage instructions.

### SimulationBuilder requires Python 3+
### Dependancies:
+   matplotlib
```powershell
C:\> pip install matplitlib
```
<hr></hr>

_Before Developing Any Simulations_
-   Ensure All files are up to date with *'git pull'*
-   All stable builds will be avaliable on the *Master Branch*
-   Advanced features can be found on the *Gui-Development Branch*
-   For the best user experience it is recomended the *Master Branch* is used

<hr></hr>

__Simulation Builder__ is developed to allow for the easy creation of *Monte Carlo* simulations from which random numbers are drawn from a **Standard Normal Distribution**.

# Basic Usage

### When creating a new simulation it is recomented that a new python file is  created. From here the SimulationBuilder module can be imported by

```python
import SimulationBuilder
```

# Predictors

### A __predictor__ allows for an array of *means* to be mapped to an array of *standard deviations*. Allowing for the ability to generate an array of floating point integers, __randomly__ selected from the standard normal distribution of the means and standard deviations associated with each Predictor object.   

### Predictors can be created by:

```python
SamplePredictor = SimulationBuilder.Predictor("Predictor-Name", [1.0, 2.0, 3.0], [1.0, 1.0, 1.0])
```

# Models

### A model allows for two __predictors__ to be grouped into Base-Deficit pairing object. A model calulates the mean of the **BasePredictor** - **DeficitPredictor** over the given number of generations.

### Models can be created by:

```python
SampleModel = SimulationBuilder.Model(BasePredictor, DeficitPredictor, 100)
```

# Simulations

### A simulation allows for a model to be generated a set number of time. From here the mean and variance can of the model can be calculated and graphed.

### Simulations can be created by:

```python
SampleSimulation = SimulationBuilder.Simulation(SampleModel, 100)
```

### To run the simulation:
```python 
SampleSimulation.simulate()
```

### To plot the simulation, ensure that the simulate method as described above has been invoked.
### Then call:

```python
SampleSimulation.plot()
```
### This will create a new figure. Once you have ploted all simulations required, the figure can be displayed by:
```python
SimulationBuilder.Simulation.show()
```
# Working example
```python
import SimulationBuilder

Normal_Growth_Predictor = SimulationBuilder.Predictor("Normal Growth Predictor",
                                                      [50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13],
                                                      [5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11]
                                                     )

Diarrhoea_Predictor = SimulationBuilder.Predictor("Diarrhoea Predictor",
                                                  [9.2, 9.2, 9.2, 9.2, 8.0, 7.6, 7.4, 6.9, 6.6, 6.2, 5.0, 5.0, 5.0, 5.0, 5.0, 4.0, 4.0, 4.0, 4.0, 4.0],
                                                  [0.5, 0.5, 0.5, 0.5, 1.1, 1.1, 1.5, 1.8, 2.1, 2.2, 2.6, 2.9, 3.5, 3.9, 4 ,4.2, 4.2, 4.2, 4.2,  4.2 ] 
                                                 )


Diarrhoea_Model = SimulationBuilder.Model(Normal_Growth_Predictor, Diarrhoea_Predictor, 100)
Diarrhoea_Simulation = SimulationBuilder.Simulation(Diarrhoea_Model, 1000)
Diarrhoea_Simulation.simulate()
Diarrhoea_Simulation.plot()
SimulationBuilder.Simulation.show()
```

![](Graph-Pictures/Example_Graph.png)

<h1 id="plotexample">Plotting Options</h1>

### Changes to how the plotting function works now allow for direct MatPlotLib commands to be passed to the plot function.

``` python
Diarrhoea_Simulation.plot(color='r', linewidth=1)
```

There remains the two optional commands
```Python
posVarColor="g" 
negVarColor="r"
```
Which by default are set to green and red but can be updated by

```Python
Diarrhoea_Simulation.plot(color='b', negVarColor="k", posVarColor="r", linewidth=1)
```

## Figure Customisation

### The new method getPlot() allows for the direct MatPlotLib "plt" variable to be accessed.

This can allow editing of features such as the xticks and yticks.

```Python
plt = Diarrhoea_Simulation.getPlot()
plt.xticks(SimulationBuilder.frange(0.5,10.5,0.5))
plt.yticks([50,40,30,20,10])
```

![](Graph-Pictures/Example_Graph2.png)

<h1 id="variance">Variance Example</h1>

### To enable variance the *variance* argument in simulate must be set to __True__

```python
Diarrhoea_Simulation.simulate(variance=True)
```
![](Graph-Pictures/Example_Graph3.png)

### The variance plots color can be changed through the options argument as shown below.

```python
Diarrhoea_Simulation.plot(color='r', negVarColor="k", posVarColor="k", linewidth=1)
```
![](Graph-Pictures/Example_Graph4.png)

<h1 id="multiplots">Multiple Plots</h1>

### Multiple plots on the same figure can be generated by invoking the plot method associated with each **Simulation** object before the **show** method is called.

```python
import SimulationBuilder

Normal_Growth_Predictor = SimulationBuilder.Predictor("Normal Growth Predictor",
                                                      [50.88, 36.63, 31.19, 27.93, 23.99, 20.97, 20.18, 19.74, 18.89, 19.11, 19.43, 18.32, 18.18, 18.93, 18.89, 17.74, 19.84, 19.71, 17.53, 15.13],
                                                      [5.19,  5.81,  6.91, 7.45, 13.02, 9.58,  9.79,  10.93, 11.08, 12.06, 12.75, 13.97, 15.41, 15.39, 17.34, 17.68, 18.52, 19.09, 19.44, 19.11]
                                                     )

Diarrhoea_Predictor = SimulationBuilder.Predictor("Diarrhoea Predictor",
                                                  [9.2, 9.2, 9.2, 9.2, 8.0, 7.6, 7.4, 6.9, 6.6, 6.2, 5.0, 5.0, 5.0, 5.0, 5.0, 4.0, 4.0, 4.0, 4.0, 4.0],
                                                  [0.5, 0.5, 0.5, 0.5, 1.1, 1.1, 1.5, 1.8, 2.1, 2.2, 2.6, 2.9, 3.5, 3.9, 4 ,4.2, 4.2, 4.2, 4.2,  4.2 ] 
                                                 )

No_Effect = SimulationBuilder.Predictor("Null",[0]*20, [0]*20)



Diarrhoea_Model = SimulationBuilder.Model(Normal_Growth_Predictor, Diarrhoea_Predictor, 100)
NormalGrowthModel = SimulationBuilder.Model(Normal_Growth_Predictor, No_Effect, 100)

Diarrhoea_Simulation = SimulationBuilder.Simulation(Diarrhoea_Model, 1000)
NormalGrowthSimulation = SimulationBuilder.Simulation(NormalGrowthModel, 1000)

Diarrhoea_Simulation.simulate()
Diarrhoea_Simulation.plot(color="r")
NormalGrowthSimulation.simulate()
NormalGrowthSimulation.plot(color="g")

SimulationBuilder.Simulation.show()
```

![](Graph-Pictures/Example_Graph5.png)

### Options can still be passed to multiple plots. However, spectific options for the figure such as the Title, and Labels should only be invoked on the last plot to avoid the risk of overwriting them.