# ------------------------------------------------------------
# Courtney Woolston
# December 21, 2018
# Modelling tibia growth for Anthro 796
# ------------------------------------------------------------

# Maresh VF Simulation: severe, moderate, and mild stress levels
  
# Import program designed to create the model
import SimulationBuilder

#-------------------------Section 1-------------------------
# Severe stress 

# Define baseline data for normal growth
Normal_Growth = SimulationBuilder.Predictor("Normal Growth",
                                                      [4.34, 3.46, 2.93, 2.62, 2.30, 2.07, 1.98, 1.95, 1.95, 1.89, 1.82, 1.75, 1.71, 1.66, 1.68, 1.64, 1.69, 1.68, 1.66, 1.72],
                                                      [0.49, 0.45, 0.36, 0.27, 0.22, 0.29, 0.21, 0.19, 0.24, 0.28, 0.18, 0.21, 0.29, 0.25, 0.22, 0.18, 0.21, 0.21, 0.32, 0.33]
                                                     )

# Define series of growth deficits to be applied to normal growth
Growth_Deficit = SimulationBuilder.Predictor("Growth Deficit",
                                                  [0.0, 0.46, 0.43, 0.42, 0.40, 0.38, 0.36, 0.35, 0.33, 0.30, 0.28, 0.27, 0.25, 0.23, 0.21, 0.20, 0.18, 0.17, 0.15, 0.14],
                                                  [0.0, 1.27, 1.32, 1.38, 1.49, 1.53, 1.57, 1.62, 1.65, 1.70, 1.72, 1.75, 1.80, 1.82, 1.85, 1.90, 1.92, 1.94, 2.0, 2.20] 
                                                 )

# Create and run simulations of the model
Maresh_VF_Model = SimulationBuilder.Model(Normal_Growth, Growth_Deficit, 50)
Maresh_VF_Simulation = SimulationBuilder.Simulation(Maresh_VF_Model, 200)
Maresh_VF_Simulation.simulate(variance=True)
Maresh_VF_Simulation.setAgeRange(SimulationBuilder.frange(0.5,10.5,0.5))

# Specify general plotting options
plt = Maresh_VF_Simulation.getPlot()
# Change font style
plt.rcParams["font.family"] = "Times New Roman"
# Change font size
plt.rcParams.update({'font.size': 12})
plt.xticks(SimulationBuilder.frange(0.0,10.5,0.5))
plt.yticks(SimulationBuilder.frange(1.0,5.0,0.5))
plt.title('Tibia Growth Velocity Simulation - Maresh VF - Severe Stress')
plt.xlabel('Age (years)', labelpad=20)
plt.ylabel('Tibia Growth Rate (cm/year)', labelpad=20)
plt.xlim(0,10)
plt.ylim(1.0,4.5)
# Hide the right and top spines
ax = plt.subplot()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# Specify model-specific plotting options
Maresh_VF_Simulation.plot(color='k',
                            marker='o',
                            markersize=5,
                            markerfacecolor='None',
                            markeredgecolor='k',
                            markeredgewidth=0.5,
                            debug=True,
                            posVarColor='r--',
                            negVarColor='r--',
                            linewidth=0.9,
                            clip_on=False)

# Output simulation results to a CSV file
Maresh_VF_Simulation.toCSV("ss_2_maresh_vf_simulation_severe_stress")
# Output all simulation runs to a CSV file 
Maresh_VF_Simulation.cacheDump("ss_2_maresh_vf_cachedump_severe_stress")
# Plot graph
SimulationBuilder.Simulation.show()

#-------------------------Section 2-------------------------
# Moderate stress

# Define baseline data for normal growth
Normal_Growth = SimulationBuilder.Predictor("Normal Growth",
                                                      [4.34, 3.46, 2.93, 2.62, 2.30, 2.07, 1.98, 1.95, 1.95, 1.89, 1.82, 1.75, 1.71, 1.66, 1.68, 1.64, 1.69, 1.68, 1.66, 1.72],
                                                      [0.49, 0.45, 0.36, 0.27, 0.22, 0.29, 0.21, 0.19, 0.24, 0.28, 0.18, 0.21, 0.29, 0.25, 0.22, 0.18, 0.21, 0.21, 0.32, 0.33]
                                                     )

# Define series of growth deficits to be applied to normal growth
Growth_Deficit = SimulationBuilder.Predictor("Growth Deficit",
                                                  [0.0, 0.34, 0.32, 0.31, 0.30, 0.29, 0.27, 0.25, 0.24, 0.21, 0.20, 0.19, 0.17, 0.16, 0.15, 0.13, 0.12, 0.11, 0.10, 0.09],
                                                  [0.0, 1.27, 1.32, 1.38, 1.49, 1.53, 1.57, 1.62, 1.65, 1.70, 1.72, 1.75, 1.80, 1.82, 1.85, 1.90, 1.92, 1.94, 2.0, 2.20] 
                                                 )

# Create and run simulations of the model
Maresh_VF_Model = SimulationBuilder.Model(Normal_Growth, Growth_Deficit, 50)
Maresh_VF_Simulation = SimulationBuilder.Simulation(Maresh_VF_Model, 200)
Maresh_VF_Simulation.simulate(variance=True)
Maresh_VF_Simulation.setAgeRange(SimulationBuilder.frange(0.5,10.5,0.5))

# Specify general plotting options
plt = Maresh_VF_Simulation.getPlot()
# Change font style
plt.rcParams["font.family"] = "Times New Roman"
# Change font size
plt.rcParams.update({'font.size': 12})
plt.xticks(SimulationBuilder.frange(0.0,10.5,0.5))
plt.yticks(SimulationBuilder.frange(1.0,5.0,0.5))
plt.title('Tibia Growth Velocity Simulation - Maresh VF - Moderate Stress')
plt.xlabel('Age (years)', labelpad=20)
plt.ylabel('Tibia Growth Rate (cm/year)', labelpad=20)
plt.xlim(0,10)
plt.ylim(1.0,4.5)
# Hide the right and top spines
ax = plt.subplot()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# Specify model-specific plotting options
Maresh_VF_Simulation.plot(color='k',
                            marker='o',
                            markersize=5,
                            markerfacecolor='None',
                            markeredgecolor='k',
                            markeredgewidth=0.5,
                            debug=True,
                            posVarColor='r--',
                            negVarColor='r--',
                            linewidth=0.9,
                            clip_on=False)

# Output simulation results to a CSV file
Maresh_VF_Simulation.toCSV("ss_2_maresh_vf_simulation_moderate_stress")
# Output all simulation runs to a CSV file 
Maresh_VF_Simulation.cacheDump("ss_2_maresh_vf_cachedump_moderate_stress")
# Plot graph
SimulationBuilder.Simulation.show()

#-------------------------Section 3-------------------------
# Mild stress 

# Define baseline data for normal growth
Normal_Growth = SimulationBuilder.Predictor("Normal Growth",
                                                      [4.34, 3.46, 2.93, 2.62, 2.30, 2.07, 1.98, 1.95, 1.95, 1.89, 1.82, 1.75, 1.71, 1.66, 1.68, 1.64, 1.69, 1.68, 1.66, 1.72],
                                                      [0.49, 0.45, 0.36, 0.27, 0.22, 0.29, 0.21, 0.19, 0.24, 0.28, 0.18, 0.21, 0.29, 0.25, 0.22, 0.18, 0.21, 0.21, 0.32, 0.33]
                                                     )

# Define series of growth deficits to be applied to normal growth
Growth_Deficit = SimulationBuilder.Predictor("Growth Deficit",
                                                  [0.0, 0.25, 0.24, 0.23, 0.22, 0.21, 0.20, 0.19, 0.18, 0.16, 0.15, 0.14, 0.12, 0.11, 0.10, 0.09, 0.08, 0.07, 0.06, 0.05],
                                                  [0.0, 1.27, 1.32, 1.38, 1.49, 1.53, 1.57, 1.62, 1.65, 1.70, 1.72, 1.75, 1.80, 1.82, 1.85, 1.90, 1.92, 1.94, 2.0, 2.20] 
                                                 )

# Create and run simulations of the model
Maresh_VF_Model = SimulationBuilder.Model(Normal_Growth, Growth_Deficit, 50)
Maresh_VF_Simulation = SimulationBuilder.Simulation(Maresh_VF_Model, 200)
Maresh_VF_Simulation.simulate(variance=True)
Maresh_VF_Simulation.setAgeRange(SimulationBuilder.frange(0.5,10.5,0.5))

# Specify general plotting options
plt = Maresh_VF_Simulation.getPlot()
# Change font style
plt.rcParams["font.family"] = "Times New Roman"
# Change font size
plt.rcParams.update({'font.size': 12})
plt.xticks(SimulationBuilder.frange(0.0,10.5,0.5))
plt.yticks(SimulationBuilder.frange(1.0,5.0,0.5))
plt.title('Tibia Growth Velocity Simulation - Maresh VF - Mild Stress')
plt.xlabel('Age (years)', labelpad=20)
plt.ylabel('Tibia Growth Rate (cm/year)', labelpad=20)
plt.xlim(0,10)
plt.ylim(1.0,4.5)
# Hide the right and top spines
ax = plt.subplot()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# Specify model-specific plotting options
Maresh_VF_Simulation.plot(color='k',
                            marker='o',
                            markersize=5,
                            markerfacecolor='None',
                            markeredgecolor='k',
                            markeredgewidth=0.5,
                            debug=True,
                            posVarColor='r--',
                            negVarColor='r--',
                            linewidth=0.9,
                            clip_on=False)

# Output simulation results to a CSV file
Maresh_VF_Simulation.toCSV("ss_2_maresh_vf_simulation_mild_stress")
# Output all simulation runs to a CSV file 
Maresh_VF_Simulation.cacheDump("ss_2_maresh_vf_cachedump_mild_stress")
# Plot graph
SimulationBuilder.Simulation.show()

#-------------------------End of Python Script-------------------------